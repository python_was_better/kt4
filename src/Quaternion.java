import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

/** Quaternions. Basic operations. */
public class Quaternion {

   private static final double DELTA = 0.000001D;
   private static final String REGEX_PATTERN =
     "^(-?\\d+(?>\\.\\d+)?)([+-]\\d+(?>\\.\\d+)?)i([+-]\\d+(?>\\.\\d+)?)j([+-]\\d+(?>\\.\\d+)?)k$";

   private final double realPart;
   private final double i;
   private final double j;
   private final double k;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.realPart = a;
      this.i = b;
      this.j = c;
      this.k = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return realPart;
   }

   /** Imaginary part i of the quaternion.
    * @return imaginary part i
    */
   public double getIpart() {
      return i;
   }

   /** Imaginary part j of the quaternion.
    * @return imaginary part j
    */
   public double getJpart() {
      return j;
   }

   /** Imaginary part k of the quaternion.
    * @return imaginary part k
    */
   public double getKpart() {
      return k;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      return format("%s%si%sj%sk", realPart, i < 0 ? i : "+" + i, j < 0 ? j : "+" + j, k < 0 ? k : "+" + k);
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      Pattern correctPattern = Pattern.compile(REGEX_PATTERN);
      Matcher matcher = correctPattern.matcher(s);

      if (!matcher.find()) {
         throw new IllegalArgumentException(format("Incorrect quaternion pattern: '%s'", s));
      }

      return new Quaternion(tryParseDouble(matcher.group(1)), tryParseDouble(matcher.group(2)),
        tryParseDouble(matcher.group(3)), tryParseDouble(matcher.group(4)));
   }

   private static double tryParseDouble(String value) {
      try {
         return Double.parseDouble(value);
      } catch (Exception e) {
         throw new RuntimeException(format("Could not convert value '%s' to Double", value));
      }
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(realPart, i, j, k);
   }

   /** Test whether the quaternion is zero.
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return doubleEquals(realPart, 0D) && doubleEquals(i, 0D) && doubleEquals(j, 0D) && doubleEquals(k, 0D);
   }

   /** Conjugate of the quaternion. Expressed by the formula
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(realPart, -i, -j, -k);
   }

   /** Opposite of the quaternion. Expressed by the formula
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-realPart, -i, -j, -k);
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(q.realPart + realPart, q.i + i, q.j + j, q.k + k);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      double realPartProduct = realPart * q.realPart - i * q.i - j * q.j - k * q.k;
      double iProduct = realPart * q.i + i * q.realPart + j * q.k - k * q.j;
      double jProduct = realPart * q.j - i * q.k + j * q.realPart + k * q.i;
      double kProduct = realPart * q.k + i * q.j - j * q.i + k * q.realPart;

      return new Quaternion(realPartProduct, iProduct, jProduct, kProduct);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(realPart * r, i * r, j * r, k * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if (isZero()) {
         throw new RuntimeException("Cannot divide by zero");
      }
      double product = realPart * realPart + i * i + j * j + k * k;
      return new Quaternion(realPart / product, -i / product, -j / product, -k / product);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return plus(q.opposite());
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException("Cannot divide by zero");
      }
      return times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException("Cannot divide by zero");
      }
      return q.inverse().times(this);
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if (!(qo instanceof Quaternion)) {
         return false;
      }

      Quaternion object = (Quaternion) qo;
      return doubleEquals(object.realPart, realPart) && doubleEquals(object.i, i)
        && doubleEquals(object.j, j) && doubleEquals(object.k, k);
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return times(q.conjugate()).plus(q.times(conjugate())).times(0.5D);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Double.valueOf(realPart).hashCode() * 123 + Double.valueOf(i).hashCode() * 234
        + Double.valueOf(j).hashCode() * 345 + Double.valueOf(k).hashCode() * 456;
   }

   /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(realPart * realPart + i * i + j * j + k * k);
   }

   // https://stackoverflow.com/questions/8081827/how-to-compare-two-double-values-in-java
   private boolean doubleEquals(double val1, double val2) {
      return Math.abs(val1 - val2) < DELTA;
   }
}